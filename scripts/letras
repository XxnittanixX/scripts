#!/bin/sh

# Get lyrics of a searched song
# Usage:
#	letras <search strings>
# Example:
#	letras beatles jude
# Dependencies: curl,lynx,sed
set -x
[ "$1" ] || {
	while [ "$REPLY" = "" ]; do
		printf "Search Song: "
		read -r REPLY
	done
	set -- $REPLY
}

search_string=$(tr ' ' '+' <<EOF
$*
EOF
)

#base_url="https://search.azlyrics.com/search.php?q="
base_url="https://search.azlyrics.com/?q="
user_agent="Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.60 Safari/537.36"

match_cmd() {
	grep -o "https://www.azlyrics.com/lyrics/.*\/.*\.html"
}

lyrics_pages=$(curl "${base_url}${search_string}" \
  -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9' \
  -H 'Accept-Language: en' \
  -H 'Cache-Control: max-age=0' \
  -H 'Connection: keep-alive' \
  -H 'DNT: 1' \
  -H "Referer: ${base_url%/*}" \
  -H 'Sec-Fetch-Dest: document' \
  -H 'Sec-Fetch-Mode: navigate' \
  -H 'Sec-Fetch-Site: same-origin' \
  -H 'Sec-Fetch-User: ?1' \
  -H 'Sec-GPC: 1' \
  -H 'Upgrade-Insecure-Requests: 1' \
  -H "User-Agent: ${user_agent}" \
  --compressed 2>/dev/null \
  | match_cmd
) || {
	>&2 printf "Error resolving\n"
	exit 2
}

if ! [ "$lyrics_pages" ]; then
	>&2 printf "No results found\n"
	exit 3
fi

lyrics_page=$(head -1 <<EOF
$lyrics_pages
EOF
)

lynx -force_html -dump -nolist -useragent="$user_agent" "$lyrics_page" 2>/dev/null \
	| sed -E '1,7d; /Submit Corrections/,$d; s/^\s+//'
