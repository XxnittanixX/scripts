#!/bin/sh
# Change alacritty's background color and opacity

usage() {
	cat <<EOF
alacritty-color '#hexcolor' '%opacity'  # Change bgcolor and opacity
alacritty-color '#hexcolor'             # Change bgcolor
alacritty-color '' '%opacity'           # Change opacity
EOF
}

case "$1" in
(-h|--help)
	usage
	exit
esac

alacritty_conf="${XDG_CONFIG_HOME:-"${HOME}/.conf"}/alacritty/alacritty.yml"

if hash yq >/dev/null 2>&1; then
	# mikefarah/yq for not removing comments
	if [ "$1" ] && [ "$2" ]; then
		bgcolor="$1" \
		opacity="$2"
		yq -i '
		.colors.primary.background = "'"$bgcolor"'" | .window.opacity = '"$opacity"'
		' "$alacritty_conf"
	elif [ "$1" ]; then
		bgcolor="$1"
		yq -i '
		.colors.primary.background = "'"$bgcolor"'"' "$alacritty_conf"
	elif [ "$2" ]; then
		opacity="$2"
		case "$opacity" in
		(*.*) opacity="0${opacity#0}"
		esac
		yq -i '
		.window.opacity = '"$opacity" "$alacritty_conf"
	else
		>&2 usage
	fi

else

	if [ "$1" ]; then
		bgcolor="$1"

		# Get the numberline of the colors.primary.background key
		n_startcolor=$(
			grep -n '^colors:' "$alacritty_conf" \
			| head -n1 \
			| cut -d: -f1
		) \
		n_startprimary=$(
			tail -n "+$n_startcolor" "$alacritty_conf" \
			| grep -E -n '\s+primary:' \
			| head -n1 \
			| cut -d: -f1
		) \
		n_bgcolor=$(
			# FIXME: en el grep no contar si empieza con #
			tail -n "+$((n_startcolor+n_startprimary))" "$alacritty_conf" \
			| grep -E -n '\s+background:' \
			| head -n1 \
			| cut -d: -f1
		) \
		n_bgcolor=$((n_bgcolor+n_startcolor+n_startprimary-1))

		# Change bg color
		sed -i -E -e "$n_bgcolor""s|(.*)'.*'(.*)|\1'""$bgcolor""'\2|" \
			"$alacritty_conf"
	fi

	if [ "$2" ]; then
		opacity="$2"
		# FIXME: not account "opacity:" starting in comment "#"
		sed -i -E -e '/opacity:/s|(.*: )[0-9\.]*(.*)|\1'"$opacity"'\2|' \
			"$alacritty_conf"
	fi
fi

