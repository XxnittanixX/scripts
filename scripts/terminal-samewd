#!/bin/sh

# Open a terminal window in the same directory as the currently active window.

: "${TERMINAL:=alacritty}"

rp(){ realpath "$@" || readlink -f "$@" ;}

# For terminals opened from an active window (e.g. GUI apps)
win_newterm(){
pid=$(xprop -id "$(xprop -root | sed -n "/_NET_ACTIVE_WINDOW/ s/^.*# // p")" | sed -n "/PID/ s/^.*= // p") || return 1
pid=$(pstree -p "$pid" | grep -o '([0-9]\+)' | tail -n 1 | tr -d '()') || return 2
cd -- "$(realpath "/proc/$pid/cwd")"
"$TERMINAL" "$@" 2>/dev/null 1>&2 &
}

# For terminals opened from another terminal
# (meaning this script is run directly as a command from the shell)
term_newterm(){
pid=$(ps -p "$$" -o 'ppid=' | tr -d ' ') || pid=$$
PWD="$(realpath "/proc/$pid/cwd")" || return 4
"$TERMINAL" "$@" 2>/dev/null 1>&2 &
}

case "$1" in
(-t)
	shift
	term_newterm "$@" || exit $?;;
('')
	win_newterm "$@" || exit $?;;
(*)
	>&2 printf "Unrecognized option: %s\n" "$1"
	exit 4
esac
