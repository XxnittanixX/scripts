#!/bin/sh
# TODO: Quiero que este sea un envolvedor general de ffmpeg
# por lo que iré agregando ideas de diferentes fuentes
# creo que lo renombraré fpeg

# Simple envolvedor de cropeador de imágenes, con ffmpeg
# Nota: considera usar imagemagick para operaciones con imágenes en vez de ffmpeg
# Ejemplo: crop.sh <fichero> 600 400

# Info del fichero pasado como $1
ruta_fichero=$(readlink -f "$1")
fichero=${ruta_fichero##*/}
fichero_sin_ext=${fichero%%.*}
extensiones=$(test "${fichero#*.}" != "$fichero" && printf ".${fichero#*.}" || printf "")

crop() {
# Nuevas proporciones de la imagen
ancho="$2"
alto="$3"
/usr/bin/ffmpeg -i "${fichero}" -filter:v scale=${ancho}:${alto} "${fichero_sin_ext}_new${extensiones}"
}
crop "$@"


# Máximos trabajos
#thread_max() {
#	njobs=0
#	for job in $(jobs -p); do
#	[ $job ] && njobs=$((njobs+1))
#	done

##	while [ $(jobs|wc l) -gt 20 ]; do
#	while [ "${njobs}" -gt 20 ]; do
#		sleep 1
#	done
#}

# Función para listar información de videos ordenadamente
info() {
	for video; do
		# Duración de video/audio, en formato HH:MM:SS.ss
		ffprobe -hide_banner "$video" 2>&1 | awk '/Duration/{ gsub(",","",$0); print $2 }' #&
		# Bitrate del video
		ffprobe -hide_banner "$video" 2>&1 | awk '/bitrate/{print $6,$7}' #&
		#thread_max
	done
}





# Función para convertir formatos
# Uso en el futuro: fpeg convert <ficheros> extension
convert() {
OPCIONES=" -hide_banner -loglevel error -stats"

eval extension=\${$#}
[ ! "$1" ] || [ ! "$2" ] && exit 2

case "$extension" in
    *.flac) OPCIONES=$OPCIONES:" -vn" ;;
    *.mov) OPCIONES=$OPCIONES:" -c:v mpeg4 -q:v 0 -pix_fmt yuv420p -c:a pcm_s16le" ;;
    *.mp4) OPCIONES=$OPCIONES:" -c:v libx264 -crf 21 -c:a libopus -b:a 320k" ;;
esac

if (grep "\." <<eof && [ ! "$3" ]
$extension
eof
)
then
    ffmpeg -i "$1" $OPCIONES "$extension"
fi

for i do
    [ "$i" != "$extension" ] && ffmpeg -i "$i" $OPCIONES "${i%.*}.$extension"
done

}
