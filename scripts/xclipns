#!/bin/sh

# Wrapper around clipboard commands, that
# sends a notification with the copied content
# Usage:
# command | xclipns   # Copy output to all found clipboards and send notification

CCn='
'

x11_clipboards="xsel xclip copyq"
wayland_clipboards="wl-copy"

pwhich() {
	hash "$1" >/dev/null 2>&1 \
		&& command -v -- "$1"
}

get_clipboards() {
	case "$XDG_SESSION_TYPE" in
		x11)
			for clipboard in $x11_clipboards; do
				clipboards="${clipboards}$(pwhich $clipboard)$CCn"
			done
			;;
		wayland)
			for clipboard in $wayland_clipboards; do
				clipboards="${clipboards}$(pwhich $clipboard)$CCn"
			done
			;;
		*)
			if [ "$DISPLAY" ]; then
				for clipboard in $x11_clipboards; do
					clipboards="${clipboards}$(pwhich $clipboard)$CCn"
				done
			elif [ "$WAYLAND_DISPLAY" ]; then
				for clipboard in $wayland_clipboards; do
					clipboards="${clipboards}$(pwhich $clipboard)$CCn"
				done
			else
				>&2 printf "No display server found\n"
				return 2
			fi
	esac
}

copy_cmd() {
	case "$1" in
		# for xsel, xclip copy to system clipboard, not primary
		*/xsel) xsel --clipboard --input ;;
		*/xclip) xclip -selection clipboard -in ;;
		*/copyq) copyq -- add "$(cat -)" ;;
			#copyq copy - ;;
		*/wl-copy) wl-copy ;;
		*) return 1 ;;
	esac
}

paste_cmd() {
	case "$1" in
		# for xsel, xclip paste from system clipboard, not primary
		*/xsel) xsel --clipboard --output ;;
		*/xclip) xclip -selection clipboard -o ;;
		*/copyq) copyq read 0 ;;
		*/wl-copy) wl-paste ;;
		*) return 1 ;;
	esac
}

main() {
	get_clipboards
	case "$1" in
		cp|'')
			stdin=$(cat -)
			# Copy stdin to all clipboards
			for clipboard in $clipboards; do
				printf -- "%s" "$stdin" | copy_cmd "$clipboard"
			done
			# Bug of notify-send? Doesn't show emojis if it's only one character
			notify-send "📋 Copiado a los portapapeles" "$stdin "
			;;
	esac
}

main "$@" || exit $?
