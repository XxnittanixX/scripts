# Scripts
Esta es una colección de mis scripts personales (principalmente escritos en
POSIX shell) para sistemas Unix, y algunos específicos para distros de Linux
basadas en Arch.

# Instalación
Estoy todavía trabajando en un Makefile y luego crearé un PKGBUILD para instalar en Arch
Algunos scripts son submódulos de git y los puedes instalar independientemente,
ver documentación en sus respectivas páginas.
Además, este repositorio será una dependencia de mi repositorio de dotfiles,
por lo que si quieres instalar todo el conjunto, ver dicha documentación.
Además he tratado de que cada script sea suficientemente independiente como
para que ***usualmente*** baste con copiarlo, hacerlo ejecutable y ejecutarlo,
si no te importara tener que instalar dependencias manualmente y que no venga
con traducciones ni documentación en manual.

# Dependencias
Como son principalmente shell scripts, tienen dependencias de varios lugares,
al empaquetarlo intentaré de que se incluyan las principales, pero también
pueden haber varias opcionales. Algunos scripts contienen un comentario con
sus dependencias o revisan si está presente y si no mandan un mensaje de error.
Las más importantes son fzf y getoptions.sh. La lista completa estará en el
fichero 'deps.csv' y es **parte de cada uno** instalar manualmente las
necesarias para cada script, ya que si no se tornaría en un paquete muy
dependiente para lo que provee que son scripts individuales.
Si desea instalarlas todas se podrá hacer con este comando:

```sh
$ pacman -S $(cut -d',' -f1 "deps.csv"|tail +2) # En Arch
```

# Uso
La mayoría de scripts muestran un mensaje de ayuda con la opción '--help',
algunos tendrán además documentación escrita en manual 'man _script_' y hay
algunos que no tienen documentación y se tiene que ver el código fuente
(que debería ser bastante corto).
Acá hay una descripción corta de lo que hace cada uno:

- `abiertos`: Muestra ficheros o ventanas abiertas
- `archivo`: Envolvedor de diferentes comandos de archivación (e.g. tar, 7z, xz, gzip, zip)
- `autocomp`: Compilación automática
- `bateria`: Muestra porcentaje de batería
- `bits`: Muestra un fichero en sus bits (ceros y unos)
- `bluefix`: Solucionador de problemas de audífonos con bluetooth
- `calcurse-notif.py`: Notificaciones para calcurse
- `caracruz.sh`: Jugar cara o cruz muchas veces o entre varios jugadores
- `caracteres`: Seleccionador de caracteres especiales como Nerd Fonts
- `change_date`: Cambiar fecha de acceso, modificación y cambio de ficheros
- `ch0`: Cambiar dueño y permisos de fichero a root
- `clearurl.bash`: Limpiar URLs sucias de metainformación de búsqueda
- `colores`: Muestra paletas de colores de tu terminal (16,256,true,escala de grises, etc.)
- `compila`: Envolvedor de compiladores e intérpretes (e.g. gcc, rustc, python, js78)
- `crop.sh`: Envolvedor de ffmpeg para cortar imágenes
- `dataframe`: Front-end de Python Pandas para conversión de formatos de tablas
- `dem`: Demonizador de comandos (dejar corriendo comandos en segundo plano)
- `drag`: Arrastrar (con el ratón) ficheros hacia cierto directorio
- `dwapk`: Descargar APKs instaladas en móvil Android hacia computador
- `emoji`: Seleccionador de emojis
- `etiquetar`: Agregar información a ficheros de audio
- `ffd`: Seleccionador de ficheros, con previsualizaciones y poder abrirlos con editor
- `find2var`: Comando 'find', pero retorna una variable con ficheros comillados
- `fman`: Seleccionador de manuales e infodocs
- `fondo-awesome`: Cambiar el fondo para awesome-wm de mis dotfiles
- `frlwrap`: Envolvedor de rlwrap que automáticamente agrega autocompletados
- `fuentes`: Listar fuentes instaladas
- `git-init-config`: Configuraciones de git al iniciar repo
- `gitd`: 'git' especificando la fecha
- `grub-dark-matter_random.sh`: Cambiar el fondo de GRUB a uno aleatorio de dark-matter
- `h`: Muestra documentación de funciones de lenguajes de programación
- `hex2rgb`: Convierte colores en hexadecimal a RGB
- `iptab`: Muestra todas tus tablas de iptables para IPv4 e IPv6
- `k`: Mata procesos interactivamente
- `lorem`: Generador de texto de ejemplo
- `lrc-shift`: Cambiar el tiempo de letras en un fichero de letras .lrc
- `lsofp`: 'lsof' pero pasándole solo PIDs
- `m4s`: Expande macros predefinidas m4 en ficheros dependiendo de su extensión
- `macrandom.sh`: Aleatoriza tus direcciones MAC
- `marcadores`: Seleccionar URLs en tus marcadores de navegadores web
- `mata`: Mata procesos de mayor consumo rápidamente
- `nom.sh`: Información del nombre de un fichero mejor formateada
- `nspawn-arch.sh`: Crear un contenedor system-nspawn de Arch, desde Arch
- `ocr`: Seleccionar región de pantalla a hacer OCR, copiarlo en portapapeles
- `pacman-backup`: Generar ficheros de respaldo para pacman
- `pacman-files`: Listar ficheros contenidos en paquetes de base de pacman
- `pacman-last`: Listar últimos paquetes instalados en base de pacman
- `pacman-others`: Listar otros comandos en mismo paquete de cierto comando
- `pacman-Qse`: Listar paquetes explícitamente instalados y descripción corta
- `pacman-which`: Muestra a qué paquete le pertenece cierto comando
- `pacmanf`: Seleccionador de paquetes de pacman
- `psel`: Seleccionador de procesos
- `punteros`: Listar ficheros de configuración de cursores (punteros)
- `qr`: Seleccionar región de pantalla a escanear código QR o de barras
- `radios`: Seleccionar radios web a escuchar
- `rgb2hex`: Convertir colores en RGB a hexadecimal
- `rshred`: Usar 'shred' recursivamente debajo de directorio(s)
- `rutas`: Listar rutas de diferentes cosas
- `sh0`: Invocar subshell que lee entrada estándar pero pudiendo establecerle su $0
- `shlocker`: Auto bloqueador de pantalla + silenciar + suspender + hibernar
- `subguionar`: Renombrar ficheros cambiándoles espacios ' ' por subguiones '_'
- `suspend-window`: Alternar suspender y des suspender ventana
- `terminal`: Muestra el nombre de la terminal actual
- `terminal-samewd`: Lanzar nueva terminal en mismo directorio
- `tiempo`: Operaciones con tiempo, incluye un cronómetro y temporizador
- `upapk`: Copiar e instalar APKs en móvil Android desde computador
- `urlkv`: Listar pares clave-valor de una URL
- `vcut`: Cortar caracteres de string como en bash
- `wifis`: Listar conexiones Wi-Fis disponibles
- `wifimenu`: Menú de selección de SSID Wi-Fi a conectar y/o (des)habilitar Wi-Fi
- `xclipns`: Envolvedor de comandos de portapapeles
- `xev-teclas`: Lanzar 'xev' y solo ver lo relevante de teclas presionadas

# Contribuir

Pido disculpas por mi inconsistencia entre escribir código en español e inglés

## Traducción
Reescribiré los mensajes de los scripts en inglés y agregaré traducciones
en español con gettext para que otros puedan contribuir en traducir a
otros idiomas en los ficheros PO.

# Licencia
La licencia es GPLv3+. Prefiero no incluir el aviso de copyright en la
mayoría de scripts pues son cortos.
