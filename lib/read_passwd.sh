#!/bin/sh
# Credits: https://unix.stackexchange.com/questions/222974/ask-for-a-password-in-posix-compliant-shell
read_passwd() {
	# Prompt is first argument
	REPLY=$(
		# always read from the tty even when redirected:
		exec < /dev/tty || exit 1  # exit only needed for bash

		# save current tty settings:
		tty_settings=$(stty -g) || exit 1

		# schedule restore of the settings on exit of that subshell
		# or on receiving SIGINT or SIGTERM:
		trap 'stty "$tty_settings"' EXIT INT TERM

		# disable terminal local echo
		stty -echo || exit 1

		# prompt on tty
		printf -- "%s" "$1" > /dev/tty

		# read password as one line, record exit status
		IFS= read -r password; ret=$?

		# display a newline to visually acknowledge the entered password
		echo > /dev/tty

		# return the password for $REPLY
		printf -- '%s' "$password"
		exit "$ret"
	) || return $?
}
