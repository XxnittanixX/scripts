#!/bin/sh
alias %=procsub
#Taken from https://github.com/modernish/modernish
#Copyright (c) 2015-2022 Martijn Dekker <martijn@inlv.org>
procsub() {
unset -v _procsub_o
while case ${1-} in
	-o) _procsub_o= ;;
	--) shift; break;;
	*) break
esac; do
	shift
done
[ $# = 0 ] && return 3
[ $1 = '' ] && return 4
: & _fifo="${TMPDIR:-"/tmp"}/fifo_${$}_$!"
until (exec mkfifo "${_fifo}") 2>/dev/null; do
[ $? -ne 0 ] && return 2
done 1>&1
(command : 1>&2 && exec 1>&2 || exec 1>&-;
command trap "PATH=/bin:/usr/bin; unset -f rm; exec rm -f ${_fifo} 2>/dev/null" 0 PIPE INT TERM;
case ${1-} in
	command) shift
		[ "${_procsub_o+a}" ] \
			&& command "$@" < "${_fifo}" \
			|| command "$@" > "${_fifo}"
	;;
	*)
		[ "${_procsub_o+a}" ] \
			&& "$@" < "${_fifo}" \
			|| "$@" > "${_fifo}"
	;;
esac) & printf "%s\n" "${_fifo}"
}
