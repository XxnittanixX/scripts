#! /bin/sh
#"\033[#mtexto\033[0m"           Formateo clásico: paleta-16 y estilo
#"\033[38;5;#mtexto\033[0m"      1°plano, paleta-256
#"\033[48;5;#mtexto\033[0m"      2°plano, paleta-256
#"\033[38;2;#;#;#mtexto\033[0m"  1°plano, paleta verdadero-color
#"\033[48;2;#;#;#mtexto\033[0m"  2°plano, paleta verdadero-color

#TODO: Quiero que este script sea más general y que pueda manejar
#a la vez colores256, rgb, hex y estilos, como ble-face. Como referencia
#puedes ver estas funciones:
#ble/color/setface
#ble/color/setface/.spec2g
#ble/color/gspec2g
#ble/color/.name2color ← esta es importante
#ble/color/g#setfg

c256() {
[ "$2" ] && set -- 2 "$@" || set -- 1 "$@"
case ${1} in
	1) printf "\033[38;5;%sm" "$(($2%256))" ;;
	2) printf "\033[48;5;%sm" "$(($2%256))" ;;
esac
}

rgb() {
#case "$1" in
#*,*) # rgb
#IFS=,
#set -- $*
#*#*) #hex
[ "$4" ] && set -- 2 "$@" || set -- 1 "$@"
case ${1} in
	1) printf "\033[38;2;%s;%s;%sm" "$(($2%256))" "$(($3%256))" "$(($4%256))";;
	2) printf "\033[48;2;%s;%s;%sm" "$(($2%256))" "$(($3%256))" "$(($4%256))";;
esac
}

hex() {
[ "$2" ] \
 && set -- 2 $(hex2rgb "$1") \
 || set -- 1 $(hex2rgb "$1")
case ${1} in
	1) printf "\033[38;2;%s;%s;%sm" "$(($2%256))" "$(($3%256))" "$(($4%256))" ;;
	2) printf "\033[48;2;%s;%s;%sm" "$(($2%256))" "$(($3%256))" "$(($4%256))" ;;
esac
}

#Estilo
am_normal="[m" #Reformatear: quitar estilo/color
ab_default="[49m" # Quitar color de segundo plano
am_n="[m"
am_bold="[1m"
am_dim="[2m"
am_italic="[3m"
am_underline="[4m"
am_blink="[5m"
am_rapidblink="[6m"
am_reverse="[7m"
am_hidden="[8m"
am_crossed="[9m"
am_overline="[53m"

#1°plano, paleta-16, colores normales
af_black="[30m"
af_red="[31m"
af_green="[32m"
af_yellow="[33m"
af_blue="[34m"
af_magenta="[35m"
af_cyan="[36m"
af_lgray="[37m"

#1°plano, paleta-16, colores claros
af_gray="[90m"
af_lred="[91m"
af_lgreen="[92m"
af_lyellow="[93m"
af_lblue="[94m"
af_lmagenta="[95m"
af_lcyan="[96m"
af_white="[97m"
af_default="[39m"

#2°plano, paleta-16, colores normales
ab_black="[40m"
ab_red="[41m"
ab_green="[42m"
ab_yellow="[43m"
ab_blue="[44m"
ab_magenta="[45m"
ab_cyan="[46m"
ab_lgray="[47m"

#2°plano, paleta-16, colores claros
ab_gray="[100m"
ab_lred="[101m"
ab_lgreen="[102m"
ab_lyellow="[103m"
ab_lblue="[104m"
ab_lmagenta="[105m"
ab_lcyan="[106m"
ab_white="[107m"
