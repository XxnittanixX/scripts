# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-07-01 00:54+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: rutas:60
msgid "See paths of different things"
msgstr ""

#: rutas:61
msgid "USAGE:"
msgstr ""

#: rutas:61
msgid "[OPTIONS] [ARGS]"
msgstr ""

#: rutas:62
msgid "OPTIONS:"
msgstr ""

#: rutas:63
msgid "PATH paths (by default)"
msgstr ""

#: rutas:64
msgid "Manual paths"
msgstr ""

#: rutas:65
msgid "Systemd unit file paths"
msgstr ""

#: rutas:66
msgid "Cursor/Pointer paths"
msgstr ""

#: rutas:67
msgid "Awk PATH paths"
msgstr ""

#: rutas:68
msgid "Perl PATH paths"
msgstr ""

#: rutas:69
msgid "Python PATH paths"
msgstr ""

#: rutas:70
msgid "Jupyter paths"
msgstr ""

#: rutas:71
msgid "Lua PATH paths"
msgstr ""

#: rutas:72
msgid "KDE paths, 'h' argument to see types"
msgstr ""

#: rutas:73
msgid "Show this help"
msgstr ""
